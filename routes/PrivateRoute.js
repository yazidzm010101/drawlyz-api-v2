const Express = require("express");
const Multer = require('multer');
const TabletController = require("../controllers/TabletController");
const ParsehubController = require("../controllers/ParsehubController");
const AdminController = require("../controllers/AdminController");
const CriteriaController = require("../controllers/CriteriaController");
const {
    passwordEncryption,    
    criteriaValidation,
    tabletNormalization,
    adminVerification,
    restrictedVerification,
    uploadHandler,    
} = require("../middleware");

const Route = Express.Router();
const Upload = Multer({
    storage: Multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/uploads/')
        },
        filename: (req, file, cb) => {
            cb(null, `${Date.now()}-${file.originalname}`)
        }
    }),
});

Route.use(adminVerification(false));

Route.get("/admin/brands", TabletController.getBrand());
Route.get("/admin/tablets", TabletController.index());
Route.get(
    "/admin/tablets/recommendation",
    TabletController.getRecommendationList()
);
Route.post("/admin/tablets", Upload.single('image'), uploadHandler('image', 'uploads'));
Route.put("/admin/tablets", Upload.single('image'), uploadHandler('image', 'uploads'));
Route.post("/admin/tablets", tabletNormalization);
Route.put("/admin/tablets", tabletNormalization);
Route.post("/admin/tablets", TabletController.create());
Route.put("/admin/tablets", TabletController.update());
Route.delete("/admin/tablets", TabletController.delete());

Route.get("/admin/parsehubs", ParsehubController.index());
Route.get("/admin/parsehubs/history", ParsehubController.getSyncHistory());
Route.post("/admin/parsehubs", ParsehubController.create());
Route.put("/admin/parsehubs", ParsehubController.update());
Route.delete("/admin/parsehubs", ParsehubController.delete());
Route.get("/admin/parsehubs/lastRun", ParsehubController.fetchAPI());

Route.get("/admin/criterias", CriteriaController.index());
Route.put("/admin/criterias", criteriaValidation);
Route.put("/admin/criterias", CriteriaController.update());

Route.get("/admin/admins", AdminController.index());

Route.use(adminVerification(true));

Route.post("/admin/admins", restrictedVerification);
Route.put("/admin/admins", restrictedVerification);
Route.delete("/admin/admins", restrictedVerification);

Route.post("/admin/admins", passwordEncryption(['confirmation', 'password']));
Route.put("/admin/admins", passwordEncryption(['confirmation', 'password']));

Route.post("/admin/admins", AdminController.create());
Route.put("/admin/admins", AdminController.update());
Route.delete("/admin/admins", AdminController.delete());

module.exports = Route;
