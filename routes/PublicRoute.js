const Express = require("express");

const AuthController = require("../controllers/AuthController");
const TabletController = require("../controllers/TabletController");
const CriteriaController = require("../controllers/CriteriaController");

const Route = Express.Router();

Route.get("/admin/me", AuthController.getProfile());
Route.post("/admin/signIn", AuthController.signIn());
Route.post("/admin/signOut", AuthController.signOut());

Route.get("/brands", TabletController.getBrand());
Route.get("/tablets", TabletController.index());
Route.get("/criterias", CriteriaController.indexPublic());
Route.post("/scores", TabletController.getRecommendationList())

module.exports = Route;

