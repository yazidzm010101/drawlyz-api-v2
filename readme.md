## .env Configuration
```ini
DATABASE_URI= # url access to mongodb server
BASE_URL= # domain name of application without slash
DATABASE_MODE= # if the value is development, then database will be reseted and filled with premade data
SESSION_SECRET= # secret key used for session data
BCRYPT_SALT_ROUNDS= # salt rounds for bcrypt
PORT=# port used to application
```