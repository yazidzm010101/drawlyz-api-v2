// models
const Admin = require("../models/Admin");
const Criteria = require("../models/Criteria");
const Tablet = require("../models/Tablet");
const Parsehub = require("../models/Parsehub");
const Rate = require("../models/Rate");
const SyncHistory = require("../models/SyncHistory");
const TabletUtils = require("../utils/TabletUtils");

// utils
const Log = require("loglevel-colors")("Test", "INFO");
const Fs = require("fs");

async function fillCollection(Model, filedir, isReset, ratio, modify) {
    try {
        const { name: collectionName } = Model.collection;
        const fulldir = process.cwd() + filedir;

        if (isReset) {
            Log.info(`Emptying ${collectionName}`);
            await Model.deleteMany();            

            Log.info(`Syncing ${collectionName}'s index`);
            await Model.syncIndexes();
        }

        Log.info(`Reading ${filedir}`);
        Fs.readFile(fulldir, "utf-8", async (err, content) => {
            var json = JSON.parse(content);
            var total = 0;

            if (ratio != undefined) {
                json = json.filter(item => item.featuredControls.match(/key/gi) || !item.featuredControls.match(/tilt/gi));
                var json_pendis = json.filter(item => item.type == 'Pen Display');
                var json_pentab = json.filter(item => item.type != 'Pen Display');
                json_pendis = json_pendis.slice(0, Math.ceil(json_pendis.length * ratio));
                json_pentab = json_pentab.slice(0, Math.ceil(json_pentab.length * ratio));
                json = [...json_pentab, ...json_pendis];
            }

            
            for (const item of json) {                    
                const newJson =  typeof(modify) == 'function' ? modify(item) : [item];
                
                await Promise.all(newJson.map(async item => {
                    try {
                        await Model.create(item);
                        total++;
                    } catch (err) {
                        Log.error(err);
                    }
                }));                               
            }
                        
            Log.info(`${total} ${collectionName} successsfully created from ${fulldir}`);
        });
    } catch (err) {
        Log.error("error filling database");
        Log.error(err);
    }
}

module.exports = {
    fillDatabase: async () => {
        /* reset sync history */
        await SyncHistory.deleteMany();
        await SyncHistory.syncIndexes();

        /* reset and fill non tablet */
        await Promise.all([
            fillCollection(Admin, "/resource/json/admin.json", true),
            fillCollection(Criteria, "/resource/json/criteria.json", true),
            fillCollection(Parsehub, "/resource/json/parsehub.json", true),
            fillCollection(Rate, "/resource/json/rate.json", true)
        ]);

        /* reset and fill tablet */
        await Promise.all([
            fillCollection(
                Tablet,
                "/resource/json/scraping/store.huion.com.json",
                true,
                1,
                TabletUtils.extractTablet
            ),
            fillCollection(
                Tablet,
                "/resource/json/scraping/store.xppen.com.json",
                false,
                1,
                TabletUtils.extractTablet
            ),
            fillCollection(
                Tablet,
                "/resource/json/scraping/gaomon.net.json",
                false,
                1,
                TabletUtils.extractTablet
            ),
            fillCollection(
                Tablet,
                "/resource/json/scraping/store.gaomon.net.json",
                false,
                1,
                TabletUtils.extractTablet
            ),

            fillCollection(
                Tablet,
                "/resource/json/scraping/store.wacom.sg.json",
                false,
                0.18,
                (item) => TabletUtils.extractTablet(item, false)
            )
        ]);
    },
};
