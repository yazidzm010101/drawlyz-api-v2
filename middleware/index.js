// model
const Admin = require("../models/Admin");

// utils
const { round, pow, sqrt } = Math;
const Log = require("loglevel-colors")("Middleware", "INFO");
const Bcrypt = require("bcrypt");

module.exports = {
    adminVerification: (isMaster) => async (req, res, next) => {
        try {
            const { user } = req.session;

            if (user) {
                const exist = await Admin.findOne({
                    username: user.username,
                    isMaster: user.isMaster,
                }).lean();

                if (exist.isMaster || !isMaster) {
                    req.session.user.name = exist.name;
                    req.session.user.username = exist.username;                    
                    return next();
                }
            }
        } catch (err) {
            Log.error(err);
        }
        return res.status(400).json({ message: "Unauthorized request" });
    },

    passwordEncryption: (keys) => async (req, res, next) => {
        try {
            const salt = parseInt(process.env.BCRYPT_SALT_ROUNDS);            

            console.log(req.body);
            for await (const key of keys) {
                if (req.body[key]) {                    
                    req.body[key] = await Bcrypt.hash(req.body[key], salt);                    
                }
            }

            return next();            
        } catch (err) {
            Log.error("error encrypting password");
            Log.error(err);
        }
        return res.status(400).json({ messsage: "Failed encrypting password" });
    },

    restrictedVerification: async (req, res, next) => {
        try {
            const { confirmation } = req.body;
            const {
                user: { username },
            } = req.session;

            if (confirmation && username) {
                const isExist = await Admin.findOne({ username });
                if (isExist) {
                    const isMatch = await Bcrypt.compare(
                        confirmation,
                        isExist.password
                    );
                    if (isMatch) {
                        return next();
                    }
                }
                return res.status(403).json({ message: "Wrong password" });
            }
        } catch (err) {
            Log.error("error verifying admin on restricted action");
            Log.error(err);
        }
        return res.status(403).json({ message: "Unauthorized request" });
    },

    uploadHandler: (field, directory) => (req, res, next) => {
        if (req.file) {
            const { filename } = req.file;
            const baseUrl = process.env.BASE_URL;
            req.body[field] = `${baseUrl}/${directory}/${filename}`;
        }
        return next();
    },

    tabletNormalization: async (req, res, next) => {
        const { workingAreaX, workingAreaY, featuredControls } = req.body;

        var workingAreaDiagonalInch =
            sqrt(pow(workingAreaX, 2) + pow(workingAreaY, 2)) / 25.4;
        workingAreaDiagonalInch = round(workingAreaDiagonalInch * 10) / 10;

        var totalFeaturedControls = 0;
        if (featuredControls) {
            featuredControls.split("+").forEach((item) => {
                const re = /^([0-9]+).*/;
                const number = item.match(re);
                if (number)
                    totalFeaturedControls += number[1];
                else
                    totalFeaturedControls++;                
            });
        }

        req.body = {
            ...req.body,
            workingAreaDiagonalInch,
            totalFeaturedControls,
        };

        return next();
    },

    criteriaValidation: (req, res, next) => {
        const { categories } = req.body;

        var invalid = 0;
        if (categories) {
            for (const category of categories) {                
                const { sets } = category;
                const { length } = sets;  

                for (var i = 0; i < length; i++) {
                    const { x, y } = sets[i];

                    if (x == undefined || y == undefined) {
                        invalid++;
                        break;
                    }
                    
                    if (i < length - 1) {
                        const { x: x1 } = sets[i + 1];                        
                        if (x >= x1) {
                            invalid++;
                            break;
                        }
                    }                    
                }
                
                if (invalid > 0)
                    break;        
            }            
        }

        if (invalid == 0) {
            return next();
        }

        return res
            .status(400)
            .json({ message: "Please ensure your sets is valid" });
    },
};
