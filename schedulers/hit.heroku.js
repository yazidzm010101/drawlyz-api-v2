const axios = require('axios');


async function recursiveHit(i = 1) {
    console.log('Hit-' + i + ' waiting for 10 seconds');
    await setTimeout(async () => {
        try {
            const data = await axios.get("https://drawlyz-v2.herokuapp.com/api/tablets");                
            if(data.status == 200) {
                console.log('Fetch ' + data.data.length + ' data');
            } else {
                console.log('Request error with status ' + data.status);
            }
        }
        catch (err) {
            console.log('err: ' + err);
        }
        i += 1;
        recursiveHit(i);
    }, 10000);
}
(
    async () => {
        recursiveHit();
    }
)();