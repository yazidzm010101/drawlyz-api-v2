require("dotenv").config();

const mongoose = require("mongoose");
const ThirdPartyUtils = require("../utils/ThirdPartyUtils");

// connection to database
mongoose.connect(process.env.DATABASE_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Database connection error:"));
db.once("open", async () => {
    const mode = process.env.MODE || "";
    if (mode.toLowerCase() == "sync") {
        console.log("Syncing from parsehub");
        await ThirdPartyUtils.updateFromParsehub();
    }
    else if (mode.toLowerCase() == "run") {
        console.log("Running from parsehub");
        await ThirdPartyUtils.runParsehub();
    }
    else if (mode.toLowerCase() == "rates") {
        console.log("Update rates for USD and IDR");
        await ThirdPartyUtils.updateRate("USD", "IDR");
    }
    mongoose.connection.close();
});
