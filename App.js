require("dotenv").config();

const Express = require("express");
const Session = require("express-session");
const Cors = require("cors");

const Axios = require("axios");
const Mongoose = require("mongoose");

const PublicRoute = require("./routes/PublicRoute");
const PrivateRoute = require("./routes/PrivateRoute");

const Test = require("./test");
const Log = require("loglevel-colors")("App", "INFO");

// connection to database
Mongoose.connect(process.env.DATABASE_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
});

const { connection: Db } = Mongoose;

Db.on("error", console.error.bind(console, "Database connection error:"));
Db.once("open", function () {    
    Log.info("Database connection success");

    const App = Express();    

    App.enable('trust proxy');
        
    App.use((req, res, next) => {
        if (process.env.NODE_ENV == 'production' && !req.secure) {
            return res.redirect("https://" + req.headers.host + req.url);
         }
     
         next();
    })

    App.use(Cors({
        credentials: true,
        origin: [process.env.BASE_URL, process.env.REACT_APP_URL],
    }));

    // middleware app level
    App.use(
        Express.urlencoded({
            extended: true,
        })
    );
    App.use(Express.json());    

    App.use(
        Session({
            resave: true,
            saveUninitialized: true,
            name: "session",            
            secret: process.env.SESSION_SECRET,
            cookie: {
                httpOnly: false,            
                sameSite: 'lax',
                // Cookie Options
                maxAge: 60 * 60 * 1000,
            }
        })
    );

    // route

    App.use("/api", PublicRoute);
    App.use("/api", PrivateRoute);
    App.use("/", Express.static('public'));
    App.get('/*', (req, res, next) => {
        res.sendFile(__dirname + '/public/index.html');
    });

    // host the app
    const Server = App.listen(process.env.PORT, async () => {
        Log.info("Running service on http://localhost:" + process.env.PORT)        
        
        Log.info("Waking up puppeteer API");
        Axios.get(process.env.PUPPETEER_API).then(response => {
            if (response.status === 200)
                Log.info("Puppeteer server is ready");
        }).catch(err => {
            Log.error("Puppeteer server not ready");
        });

        if (process.env.DATABASE_MODE == "development") {    
            Log.info("Filling database");
            Test.fillDatabase();
        }        
        
    });

    Server.setTimeout(60000);
});
