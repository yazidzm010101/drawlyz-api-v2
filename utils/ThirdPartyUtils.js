const axios = require("axios");
const Parsehub = require("../models/Parsehub");
const Tablet = require("../models/Tablet");
const Rate = require("../models/Rate");
const SyncHistory = require("../models/SyncHistory");
const TabletUtils = require("./TabletUtils");
const Log = require("loglevel-colors")("ThirdPartyUtils", "Info");

async function updateRate(base, target) {
    return new Promise((resolve, reject) => {
        base = base.toUpperCase();
        target = target.toUpperCase();
        axios
            .get(
                `https://free.currconv.com/api/v7/convert?q=${base}_${target}&compact=ultra&apiKey=a531dc13e702ce27750f`
            )
            .then(async (response) => {
                if (response.status === 200) {
                    const amount = response.data[base + "_" + target];
                    const exist = Rate.findOne({ base, target });
                    (exist &&
                        (await Rate.updateOne({ base, target }, { amount }))) ||
                        (await Rate.create({ base, target, amount }));
                    resolve();
                }
                reject(true);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

async function runParsehub() {
    const parsehubs = await Parsehub.find().lean();

    for await (const item of parsehubs) {
        try {
            await axios.post(
                `https://www.parsehub.com/api/v2/projects/${item.projectToken}/run?api_key=${item.apiKey}`
            );
        } catch (err) {
            Log.error(err);
        }
    }
}

async function updateFromParsehub() {
    const parsehubs = await Parsehub.find().lean();
    var totalNew = 0;
    var totalUpdated = 0;
    var totalPartial = 0;
    var totalFailed = 0;
    var totalFailedRequest = 0;

    await Promise.all(
        parsehubs.map(async (item) => {
            try {
                // make request
                const response = await axios.get(
                    `https://www.parsehub.com/api/v2/projects/${item.projectToken}/last_ready_run/data`,
                    { params: { api_key: item.apiKey, format: "json" } }
                );
                // if failed
                if (response.status !== 200) {
                    throw "Request failed with status " + response.status;
                } else {
                    // extract data
                    var tablets = [];
                    for await (const tablet of response.data.pentablets) {
                        
                        tablets = [
                            ...tablets,
                            ...TabletUtils.extractTablet(tablet),
                        ];
                    }

                    // iterate through data and create/update each items
                    for await (const tablet of tablets) {
                        // if exists then update price only
                        const exists = await Tablet.findOne({
                            brand: tablet.brand,
                            model: tablet.model,
                        }).lean();
                        if (exists) {
                            const price = parseFloat(tablet.price);
                            if (price > 0 && price != exists.price) {
                                await Tablet.updateOne(
                                    {
                                        brand: tablet.brand,
                                        model: tablet.model,
                                    },
                                    { ...exists, price: tablet.price }
                                );
                                totalUpdated++;
                            }
                        } else {
                            Tablet.create(tablet, (err, data) => {
                                if (err) {
                                    Log.error(err);                                    
                                    totalFailed++;
                                } else {
                                    TabletUtils.checkPartiality(tablet)
                                        ? totalPartial++
                                        : totalNew++;
                                }
                            });
                        }
                    }
                }
            } catch (err) {
                Log.error("Error happened fetching " + item.name);
                Log.error(err);                
                totalFailedRequest++;
            }
        })
    );

    await SyncHistory.create({
        totalNew,
        totalUpdated,
        totalPartial,
        totalFailed,        
        totalFailedRequest,
    });

    return {
        totalNew,
        totalUpdated,
        totalPartial,
        totalFailed,
        totalFailedRequest,
    };
}

module.exports = { updateRate, runParsehub, updateFromParsehub };
