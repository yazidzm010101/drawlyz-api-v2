const { floatRound } = require('./MathUtils');
const Log = require("loglevel-colors")("TabletUtils", "INFO");

function extractResolution(resolution) {
    var resolutionX = 0;
    var resolutionY = 0;        
    try {
        if(resolution) {
            const regexSplit = /[ｘx*×]/i;
            const arrResolution = resolution.split(regexSplit);
            if(arrResolution.length >= 2) {
                resolutionX = parseInt(arrResolution[0]);
                resolutionY = parseInt(arrResolution[1]);                    
            }
        }
    } catch (err) {
        Log.error(err)
    }
    return { resolutionX, resolutionY }
}

function extractDimension(dimension, ratio) {
    var dimensionX = 0;
    var dimensionY = 0;
    var dimensionZ = 0;
    try {
        const regexSplit = /[ｘx*×]/i;
        const regexNumber = /[0-9]+(.[0-9]+){0,1}/;
        const arrDimension = dimension.split(regexSplit);        
        if(arrDimension.length >= 3){            
            dimensionX = floatRound(parseFloat(arrDimension[0].match(regexNumber)[0]) * ratio, 2);
            dimensionY = floatRound(parseFloat(arrDimension[1].match(regexNumber)[0]) * ratio, 2);
            dimensionZ = floatRound(parseFloat(arrDimension[2].match(regexNumber)[0]) * ratio, 2);
        }
    }
    catch (err) {        
        Log.error(err)            
    }
    return { dimensionX, dimensionY, dimensionZ };
}

function extractWorkingArea(workingArea, ratio, resolutionX, resolutionY) {
    var workingAreaDiagonalInch = 0;
    var workingAreaX = 0;
    var workingAreaY = 0;
    try {
        const regexSplit = /[ｘx*×]/i;
        const regexNumber = /[0-9]+(.[0-9]+){0,1}/;
        const arrWorkingArea = workingArea.split(regexSplit);

        // if workingArea contain two axes
        if(arrWorkingArea.length >= 2) {
            workingAreaX = floatRound(parseFloat(arrWorkingArea[0].match(regexNumber)[0]) * ratio, 2);
            workingAreaY = floatRound(parseFloat(arrWorkingArea[1].match(regexNumber)[0]) * ratio, 2);
            workingAreaDiagonalInch = floatRound(Math.sqrt(Math.pow(workingAreaX / 25.4, 2) + Math.pow(workingAreaY / 25.4, 2)) ,2);
        
        // for pen display if workingArea contain diagonal in inch and have specific resolution
        } else if (resolutionX && resolutionY) {
            workingAreaDiagonalInch = floatRound(workingArea.match(/[0-9]+(.[0-9]+){0,1}/)[0], 2);
            const aspectRatio = resolutionX / resolutionY;
            const h = Math.sqrt(Math.pow(workingAreaDiagonalInch, 2) / (Math.pow(aspectRatio, 2) + 1));
            workingAreaX = floatRound(aspectRatio * h * ratio, 2)
            workingAreaY = floatRound(h * ratio, 2);
        }
    }
    catch (err) {
        Log.error(err)        
    }
    return { workingAreaX, workingAreaY, workingAreaDiagonalInch };
}

function extractTotalKeys(featuredControls) {
    var totalKeys = 0;
    featuredControls.split("+").forEach((func) => {
        if (func.match(/^([0-9]+).*key/gi) != null) {
            totalKeys += parseInt(func.match(/^([0-9]+).*/)[1]);
        }        
    });
    return totalKeys;
}

function extractTiltRecognitionSupport(featuredControls) {
    var tiltRecognitionSupport = 0;
    if(featuredControls.match(/tilt/gi)) {
        tiltRecognitionSupport = 1;
    }
    return tiltRecognitionSupport;
}

function extractRadialKey(featuredControls) {
    var radialKey = 0;
    if(featuredControls.match(/(wheel|ring|radial)/gi)) {
        radialKey = 1;
    }
    return radialKey;
}
    
function extractPressureLevels(str) {
    var pressureLevels = 0;
    try {
        const regexPattern = /[0-9]+([.,][0-9]+){0,1}/;
        const pressureLevelsArr = str.match(regexPattern);
        pressureLevels = parseInt(pressureLevelsArr[0]);
    } catch (err) {
        Log.error(err)
    }
    return pressureLevels;
}    

function extractPrice(str, index) {
    var price = 0;
    try {
        const priceArr = str.split(";").sort((a, b) => (a < b && 1) || -1);
        if(priceArr.length > 1) {
            price = floatRound(priceArr[index], 2)
        } else if (priceArr.length == 1){
            price = floatRound(priceArr[0], 2);
        }                
    }
    catch (err) {
        Log.error(err)
    }
    return price;
}

function extractTablet(item, splitModel = true) {
    try {

        // determine rate rather in inch or mm        
        var dimensionRate;
        var workingAreaRate;
        var data = [];
                
        dimensionRate = item.dimension.match(/inch/i) != null ? 25.4 : 1;
        workingAreaRate = item.workingArea.match(/inch/i) != null ? 25.4 : 1;
        
    
        // split if data have size variation
        const models = splitModel ? item.model.split(/\s*[/]\s*/) : [item.model];        
        const sizeGroup = item.dimension.match(/\w*:/g) || [""];
        const dimensionGroup = item.dimension.split(/\s+\w*:/g) || [item.dimension];
        const workingAreaGroup = item.workingArea.split(/\s+\w*:/g) || [item.workingArea];
    
        
        models.forEach(baseModel => {
            sizeGroup.forEach((size, index) => {
                baseModel = baseModel.replace(/\s+(\w*[/])+\w/g, '');

                const model = size ? baseModel + " " + size.replace(/:\s*/, "") : baseModel;
                const dimension = dimensionGroup[index];
                const workingArea = workingAreaGroup[index];
                
                const price = extractPrice(item.price, index);
                const pressureLevels = extractPressureLevels(item.pressureLevels);
                const { resolutionX, resolutionY } = extractResolution(item.resolution);
                const { dimensionX, dimensionY, dimensionZ } = extractDimension(dimension, dimensionRate);
                const { workingAreaX, workingAreaY, workingAreaDiagonalInch } = 
                    extractWorkingArea(workingArea, workingAreaRate, resolutionX, resolutionY);
                const totalKeys = extractTotalKeys(item.featuredControls);            
                const radialKey = extractRadialKey(item.featuredControls);            
                const tiltRecognitionSupport = extractTiltRecognitionSupport(item.featuredControls);
        
                data.push({
                    ...item,
                    model,
                    pressureLevels,
                    dimensionX,
                    dimensionY,
                    dimensionZ,
                    workingAreaX,
                    workingAreaY,
                    workingAreaDiagonalInch,
                    resolutionX,
                    resolutionY,
                    totalKeys,
                    radialKey,
                    tiltRecognitionSupport,
                    price,
                });
            });
        });
    
        return data;
    }
    catch (err) {
        Log.error(err);
        Log.info(item);
        return [ item ];
    }
}

function checkPartiality(tablet) {
    try {        
        return (
            (["", undefined, null].includes(tablet.pressureLevels) || tablet.pressureLevels <= 0 || tablet.pressureLevels % 1024 != 0) ||
            (["", undefined, null].includes(tablet.dimensionX) || tablet.dimensionX <= 0) ||
            (["", undefined, null].includes(tablet.dimensionY) || tablet.dimensionY <= 0) ||
            (["", undefined, null].includes(tablet.dimensionZ) || tablet.dimensionZ <= 0) ||
            (["", undefined, null].includes(tablet.workingAreaX) || tablet.workingAreaX <= 0) ||
            (["", undefined, null].includes(tablet.workingAreaY) || tablet.workingAreaY <= 0) ||
            (["", undefined, null].includes(tablet.workingAreaDiagonalInch) || tablet.workingAreaDiagonalInch <= 0) ||
            (["", undefined, null].includes(tablet.totalKeys) || tablet.totalKeys < 0) ||
            (["", undefined, null].includes(tablet.radialKey) || tablet.radialKey < 0) ||
            (["", undefined, null].includes(tablet.tiltRecognitionSupport) || tablet.tiltRecognitionSupport < 0) ||
            (["", undefined, null].includes(tablet.price) || tablet.price <= 0)
        );
    } catch (err) {
        Log.error(err)
        return true;
    }
}

module.exports = {
    extractTablet, checkPartiality
};
