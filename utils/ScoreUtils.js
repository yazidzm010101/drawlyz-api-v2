const Criteria = require("../models/Criteria");
const { floatRound, findFuzzyDegree } = require("./MathUtils");

async function populateCriteria(userCriterias) {
    /* populate categories foreach userCriterias */
    var criterias = [];
    var i = 0;
    for await (const userCriteria of userCriterias) {
        var criteria = await Criteria.findOne({name: userCriteria.name}).lean();
        if(criteria) {
            criteria.categories = criteria.categories.filter(category =>
                userCriterias[i].categories.includes(category.name)
            );
            criterias.push(criteria);
        }
        i++;        
    };

    return criterias;
}

async function calculateScore (tablet, criterias) {
    /* main variable */            
    var fireScore = 0;    

    /* populate scores into criterias */
    criterias = await Promise.all(
        criterias.map(async criteria => {
            const { name, field } = criteria;
            const categories = await Promise.all(criteria.categories.map(category => {
                const { name, sets } = category;
                const score = floatRound(findFuzzyDegree(tablet[field], sets), 2);
                return { name, score }
            }))
            return { name, categories };
        })
    );

    /* populate group score using Fuzzy OR */
    criterias = await Promise.all(
        criterias.map(criteria => {
            const scores = criteria.categories.map(category => category.score);
            const groupScore = Math.max(...scores);
            return {  ...criteria, groupScore}
        })
    );

    /* calculate fire score using Fuzzy AND */
    fireScore = Math.min(...criterias.map(criteria => criteria.groupScore));    

    return { ...tablet, fireScore, criterias };
}

module.exports = { populateCriteria, calculateScore };