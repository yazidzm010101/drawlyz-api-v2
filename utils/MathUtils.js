function floatRound (str, decimalPoint) {
    const decimal = Math.pow(10, decimalPoint);
    const number = Math.round(parseFloat(str) * decimal) / decimal;
    return number;
}

function clampValue (str, start, end) {
    const number = parseFloat(str);
    return Math.min(Math.max(number, start), end);
}

function findTwoCorresspondingPoints(sets, x) {    
    for (var i = 0; i < sets.length - 1; i++) {
        const { x: x1 } = sets[i];
        const { x: x2 } = sets[i + 1];        
        if(x >= x1 && x <= x2) {
            return [ sets[i], sets[i + 1] ];            
        }
    }
    throw Error("ensure you clamp the value within the sets first!");        
}

function linearInterpolation (sets, x) {
    const { x: x1, y: y1 } = sets[0];
    const { x: x2, y: y2 } = sets[1];
    return ((x - x1) / (x2 - x1) * (y2 - y1)) + y1;
}

function findFuzzyDegree (value, sets) {
    const { length } = sets;
    
    if (length == 1) { // if length is 1 then just check value and sets[0].x
        const { x, y } = sets[0];
        return value == x ? y : 0;
    }    
    else if (length > 1) { // if length is > 1 then find degree using linear interpolation       
        const { x: x0 } = sets[0]; // first x
        const { x: xn } = sets[length - 1]; // last x
        value = clampValue(value, x0, xn); // clamp the value        
        const correspondingSets = findTwoCorresspondingPoints(sets, value);
        return linearInterpolation(correspondingSets, value);        
    } else {
        return 0;
    }
}

module.exports = { floatRound, clampValue, findTwoCorresspondingPoints, findFuzzyDegree }