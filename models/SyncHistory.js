const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema(
    {
        totalNew: {
            type: Number,
            required: true
        },
        totalUpdated: {
            type: Number,
            required: true
        },
        totalPartial: {
            type: Number,
            required: true
        },        
        totalFailed: {
            type: Number,
            required: true
        },        
        totalFailedRequest: {
            type: Number,
            required: true
        },
    },
    {
        timestamps: true,
    }
);

module.exports = Mongoose.model("synchistory", Schema);
