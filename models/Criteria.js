const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    field: {
        type: String,
        unique: true,
        required: true,
    },
    categories: [
        {
            name: {
                type: String,
                required: true,
            },
            sets: [
                {
                    x: {
                        type: Number,
                        required: true,
                    },
                    y: {
                        type: Number,
                        required: true,
                    },
                },
            ],
        },
    ],
});

module.exports = Mongoose.model("Criteria", Schema);
