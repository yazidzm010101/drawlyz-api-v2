const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema(
    {
        type: {
            type: String,
            default: 'Pen Tablet',
        },
        brand: {
            type: String,
            required: true,
        },
        model: {
            type: String,
            required: true,
        },
        image: {
            type: String,            
            default: '',
        },
        url: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            default: 0,
        },
        pressureLevels: {
            type: Number,
            default: 0,
        },
        resolutionX: {
            type: Number,
            default: 0,
        },
        resolutionY: {
            type: Number,
            default: 0,
        },
        dimensionX: {
            type: Number,
            default: 0,
        },
        dimensionY: {
            type: Number,
            default: 0,
        },
        dimensionZ: {
            type: Number,
            default: 0,
        },
        workingAreaX: {
            type: Number,
            default: 0,
        },
        workingAreaY: {
            type: Number,
            default: 0,
        },
        workingAreaDiagonalInch: {
            type: Number,
            default: 0,
        },        
        totalKeys: {
            type: Number,
            default: 0,
        },
        radialKey: {
            type: Number,
            default: 0,
        },
        tiltRecognitionSupport: {
            type: Number,
            default: 0,
        },
        featuredControls: {
            type: String,
            default: '',
        },
    },
    {
        timestamps: true,        
    }
);

Schema.index({ brand: 1, model: 1 }, { unique: true });

module.exports = Mongoose.model("Tablet", Schema);
