const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    isMaster: Boolean,
});

module.exports = Mongoose.model("Admin", Schema);
