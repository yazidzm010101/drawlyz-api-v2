const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    projectToken: {
        type: String,
        required: true,
    },
    apiKey: {
        type: String,
        required: true,
    },    
});

module.exports = Mongoose.model("Parsehub", Schema);
