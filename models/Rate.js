const Mongoose = require("mongoose");

const Schema = new Mongoose.Schema({
    base: {
        type: String,
        required: true,
    },
    target: {
        type: String,
        required: true,
    },
    amount: {
        type: Number,
        required: true,
    },    
});

module.exports = Mongoose.model("Rate", Schema);
