const Admin = require("../models/Admin");
const Bcrypt = require("bcrypt");
const Log = require("loglevel-colors")("Auth", "INFO");

class AuthController {
    constructor() {
        this.model = Admin;
    }

    getProfile() {
        return async (req, res) => {
            try {
                const { user } = req.session;
                const { username, isMaster } = user;
                const exist = await this.model.findOne({ username, isMaster });

                if (exist)                                
                    return res.status(200).json({ user });                
                
            } catch (err) {
                Log.error("Admin profile");                              
            }
            return res
                .status(400)
                .json({ message: "Unauthorized credential" });
        };
    }

    signIn() {
        return async (req, res) => {
            try {
                Log.info("sign in");

                const { username, password } = req.body;
                const data = await this.model.findOne({ username }).lean();

                if (data) {
                    const { name, isMaster, password: hash } = data;
                    const match = await Bcrypt.compare(password, hash);
                    if (match) {
                        req.session.user = { username, name, isMaster };
                        return res.status(200).json({
                            user: req.session.user,
                            message: `Welcome back, ${data.name}!`,
                        });
                    }
                }

                return res
                    .status(403)
                    .json({ message: "Wrong username or password!" });
            } catch (err) {
                Log.error("sign in error");
                Log.error(err);
                return res.status(400).json({ message: "Sign in failed" });
            }
        };
    }

    signOut() {
        return async (req, res) => {
            req.session = null;
            return res.status(200).json({ message: "Sign out successful!" });
        };
    }
}

module.exports = new AuthController();
