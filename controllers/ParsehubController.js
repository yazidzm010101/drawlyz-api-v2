const BaseController = require("./BaseController");
const Parsehub = require("../models/Parsehub");
const ThirdPartyUtils = require("../utils/ThirdPartyUtils");
const SyncHistory = require("../models/SyncHistory");

class ParsehubController extends BaseController {
    // @override
    getModel() {
        return Parsehub;
    }

    getUniqueField() {
        return ["name"];
    }
    
    getSyncHistory() {
        return async (req, res) => {            
            this.Log.info('history');
            const data = await SyncHistory.find({}).sort([['createdAt', -1]]).lean();
            return res.status(200).json(data);            
        }
    }

    fetchAPI() {
        return async (req, res) => {
            try {
                this.Log.info('fetch API');

                const data = await ThirdPartyUtils.updateFromParsehub();
                return res.status(200).json(data);
            } catch (err) {       
                this.Log.error('fetch API');
                this.Log.error(err);
                return res
                    .status(500)
                    .json({ message: "An error happened" });
            }
        };
    }
}

module.exports = new ParsehubController();
