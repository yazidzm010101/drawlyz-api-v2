const BaseController = require("./BaseController");
const Admin = require("../models/Admin");

class AdminController extends BaseController {
    // @override
    getModel() {
        return Admin;
    }
    getUniqueField() {
        return ["username"];
    }
    getFilterField() {
        return ["_id", "name", "username", "isMaster"];
    }
}

module.exports = new AdminController();
