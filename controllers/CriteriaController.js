const BaseController = require("./BaseController");
const Criteria = require("../models/Criteria");

class CriteriaController extends BaseController {
    // @override
    getModel() {
        return Criteria;
    }

    getUniqueField() {
        return [];
    }

    indexPublic() {
        return async (req, res) => {            
            const criteria = await Criteria.aggregate([{
                "$project": {
                    "_id": 0,
                    "name": 1,
                    "categories": "$categories.name",
                }
            }]);   
            
            return res.json(criteria);
        }
    } 
}

module.exports = new CriteriaController();
