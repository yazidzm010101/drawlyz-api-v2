const BaseController = require("./BaseController");
const Tablet = require("../models/Tablet");
const Rate = require("../models/Rate");
const Axios = require("axios");
const { calculateScore, populateCriteria } = require("../utils/ScoreUtils");

class TabletController extends BaseController {
    /* @override */
    constructor() {
        super();
        this.query = [
            { model: { $ne: null } },
            { brand: { $ne: null } },
            { type: { $ne: null } },
            { pressureLevels: { $gt: 0 } },
            { pressureLevels: { $mod: [1024, 0] } },
            { dimensionX: { $gt: 0 } },
            { dimensionY: { $gt: 0 } },
            { dimensionZ: { $gt: 0 } },
            { workingAreaX: { $gt: 0 } },
            { workingAreaY: { $gt: 0 } },
            { workingAreaDiagonalInch: { $gt: 0 } },
            { totalKeys: { $gte: 0 } },
            { radialKey: { $gte: 0 } },
            { tiltRecognitionSupport: { $gte: 0 } },
            { price: { $gt: 0 } },
        ];
    }

    /* @override */
    getModel() {
        return Tablet;
    }

    /* @override */
    getUniqueField() {
        return ["model", "brand"];
    }

    index() {
        return async (req, res) => {
            try {
                this.Log.info("index");
                this.Log.info("Waking up puppeteer API");
                Axios.get(process.env.PUPPETEER_API).then(response => {
                    if (response.status === 200)
                        this.Log.info("Puppeteer server is ready");
                }).catch(err => {
                    this.Log.error("Puppeteer server not ready");
                });

                /* main variable */
                var find, tabletList;
                const { brand, incomplete } = req.query;                

                /* get rate */
                const rate = await Rate.findOne({
                    base: "USD",
                    target: "IDR",
                }).lean();

                /* determine find query */
                if (incomplete) {
                    find = { $nor: [{ $and: this.query }] };
                } else if (brand) {
                    find = { $and: [{ brand }, ...this.query] };
                } else {
                    find = { $and: this.query };
                }

                /* find data */
                tabletList = await this.model.find(find, this.filterField).lean();

                /* populate extra field */
                tabletList = await Promise.all(
                    tabletList.map((tablet) => {
                        tablet.featuredControls = tablet.featuredControls.replace(/\s*[+]\s*/g, '+');
                        tablet.priceIDR = Math.round(tablet.price * rate.amount);
                        return tablet;
                    })
                );

                return res.json(tabletList);

            } catch (err) {
                this.Log.error(err);
                return res.status(500).json({ message: "Something went wrong" });
            }
        };
    }

    getBrand() {
        return async (req, res) => {
            this.Log.info("get brand");
            const data = await this.model.find().distinct("brand");
            return res.json(data);
        };
    }

    getRecommendationList() {
        return async (req, res) => {
            try {
                this.Log.info("get recommendation list");
                this.Log.info("Waking up puppeteer API");
                Axios.get(process.env.PUPPETEER_API).then(response => {
                    if (response.status === 200)
                        this.Log.info("Puppeteer server is ready");
                }).catch(err => {
                    this.Log.error("Puppeteer server not ready");
                });

                /* main variable */
                var tabletList;                
                const criterias = await populateCriteria(req.body);

                /* get rate */
                const rate = await Rate.findOne({
                    base: "USD",
                    target: "IDR",
                }).lean();                
                
                /* find data */
                tabletList = await Tablet.find({ $and: this.query }).lean();

                /* populate extra field */
                tabletList = await Promise.all(
                    tabletList.map(async (tablet) => {
                        tablet.featuredControls = tablet.featuredControls.replace(/\s*[+]\s*/g, '+');
                        tablet.priceIDR = Math.round(tablet.price * rate.amount);
                        tablet = await calculateScore(tablet, criterias);
                        return tablet;
                    })
                );
                tabletList = tabletList.filter((item) => item.fireScore != 0);
                tabletList = tabletList.sort(
                    (a, b) => (a.fireScore > b.fireScore && -1) || 1
                );
                return res.status(200).json(tabletList);
            } catch (err) {
                this.Log.error("get recommendation list");
                this.Log.error(err);
                return res.status(400).json({ message: "invalid request" });
            }
        };
    }
}

module.exports = new TabletController();
