const Log = require('loglevel-colors');

class BaseController {
    constructor() {
        if (this.constructor === BaseController) {
            throw new Error("Can't instantiate abstract class!");
        }                
        this.model = this.getModel();        
        this.uniqueField = this.getUniqueField();
        this.filterField = this.getFilterField();
        this.Log = Log(this.model.collection.name, "INFO");
    }

    getModel() {
        throw new Error(
            "[BaseController] Method 'getModel()' must be implemented."
        );
    }

    getUniqueField() {
        return [];
    }

    getFilterField() {
        return [];
    }    

    index() {
        return async (req, res) => {
            this.Log.info('index');
            const data = await this.model.find({}, this.filterField);            
            return res.json(data);
        };
    }

    create() {
        return async (req, res) => {
            try {
                this.Log.info('create');
                
                var filter = {};
                this.uniqueField.forEach((field) => {
                    filter[field] = req.body[field];
                });

                const data = await this.model.findOne(filter);
                if (data) {                                        
                    return res
                        .status(400)
                        .json({ message: "Data already exists" });
                }

                await this.model.create(req.body);                
                return res
                    .status(200)
                    .json({ message: "Data successfully created" });

            } catch (err) {
                this.Log.error('create');
                this.Log.error(err);                
                return res.status(400).json({ message: "Failed creating data" });
            }
        };
    }

    update() {
        return async (req, res) => {
            try {
                this.Log.info('update');

                const data = await this.model.findOne({ _id: req.body._id }).lean();
                if (data) {
                    await this.model.updateOne({ _id: req.body._id }, req.body);                    
                    return res
                        .status(200)
                        .json({ message: "Data successfully updated" });
                }
                
                return res.status(400).json({ message: "Data didn't exists" });
            } catch (err) {
                this.Log.error('update');           
                this.Log.error(err);
                return res.status(400).json({ message: "Failed updating data" });
            }
        };
    }

    delete() {
        return async (req, res) => {
            try {
                this.Log.info('delete');

                const data = await this.model.findOne({ _id: req.body._id }).lean();
                if (data) {
                    await this.model.deleteOne({ _id: req.body._id });                    
                    await this.model.syncIndexes();                    
                    return res
                        .status(200)
                        .json({ message: "Data successfully deleted" });
                }
                
                return res.status(400).json({ message: "Data didn't exists" });
            } catch (err) {    
                this.Log.error('delete');
                this.Log.error(err);
                return res.status(400).json({ message: "Failed updating data" });
            }
        };
    }
}

module.exports = BaseController;
